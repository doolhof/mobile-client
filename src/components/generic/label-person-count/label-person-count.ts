import {Component, Input} from '@angular/core';

/**
 * Generated class for the LabelPersonCountComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'label-person-count',
  templateUrl: 'label-person-count.html'
})
export class LabelPersonCountComponent {
  @Input() online: number;

  constructor() {
  }

}
