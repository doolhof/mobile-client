import {Component, EventEmitter, Output} from '@angular/core';

/**
 * Generated class for the SearchbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'searchbar',
  templateUrl: 'searchbar.html'
})
export class SearchbarComponent {
  @Output() selectableItemsEmitter = new EventEmitter();
  constructor() {
  }

  search(event): void {
    this.selectableItemsEmitter.emit(event);
  }

}
