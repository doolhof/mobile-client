import { Component } from '@angular/core';

/**
 * Generated class for the NextTurnButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'next-turn-button',
  templateUrl: 'next-turn-button.html'
})
export class NextTurnButtonComponent {

  text: string;

  constructor() {
    console.log('Hello NextTurnButtonComponent Component');
    this.text = 'Hello World';
  }

}
