import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the ButtonDefaultComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'button-default',
  templateUrl: 'button-default.html'
})
export class ButtonDefaultComponent {

  @Input() label: string;
  constructor() {

  }

}
