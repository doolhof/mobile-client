import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../model/user";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Storage} from '@ionic/storage';

/**
 * Generated class for the ChatwindowComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chatwindow',
  templateUrl: 'chatwindow.html'
})
export class ChatwindowComponent implements OnInit {

  @Input() gameId: number;
  private chatStompClient;
  messageText = '';
  private user: User;
  private responseMessage: any;
  private lobbyId: number;

  constructor(private storage: Storage) {
    this.lobbyId =1;
  }

  ngOnInit(): void {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
    });
    this.chatConnect();
  }

  chatConnect() {
    const that = this;
    const socket = new SockJS('https://doolhof-backend.herokuapp.com/chat-socket');
    this.chatStompClient = Stomp.over(socket);
    this.chatStompClient.connect({}, function () {
      that.chatStompClient.send('/app/chat.addUser',
        {},
        JSON.stringify({sender: that.user.username, type: 'JOIN', lobbyId: 1}));
      that.chatStompClient.subscribe('/topic/public', (response) => {
        that.onMessageReceived(response);
      });
    });
  }

  onMessageReceived(payload) {
      this.responseMessage = JSON.parse(payload.body);
       if (this.responseMessage.lobbyId.toString() === this.lobbyId.toString()) {
      if (this.responseMessage.type.toString() === 'JOIN') {
        if (this.responseMessage.content !== undefined) {
          this.responseMessage.content = this.responseMessage.sender + ' joined!';
          this.messageText = this.responseMessage.content;
        }
      } else if (this.responseMessage.type.toString() === 'LEAVE') {
        console.log('LEAVE');
        this.responseMessage.content = this.responseMessage.sender + ' left!';
        this.messageText = this.responseMessage.content;
      } else {
        this.messageText = this.responseMessage.sender + ': ' + this.responseMessage.content;
      }
    }
  }

  sendMessage($event) {
    const chatMessage = {
      sender: this.user.username,
      content: $event.toString(),
      type: 'CHAT',
      lobbyId: this.lobbyId.toString(),
      gameId: '0'
    };
    this.chatStompClient.send('/app/chat.sendMessage', {}, JSON.stringify(chatMessage));
  }

}
