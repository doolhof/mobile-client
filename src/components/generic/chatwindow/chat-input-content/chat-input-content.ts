import {Component, EventEmitter, Output} from '@angular/core';

/**
 * Generated class for the ChatInputContentComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-input-content',
  templateUrl: 'chat-input-content.html'
})
export class ChatInputContentComponent {

  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();


  constructor() {
  }
  sendMessage($event) {
    this.messageEmitter.emit($event);
  }
}
