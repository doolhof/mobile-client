import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';

/**
 * Generated class for the ChatInputboxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-inputbox',
  templateUrl: 'chat-inputbox.html'
})
export class ChatInputboxComponent {

  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();
  @ViewChild('textArea') textInput: ElementRef;

  constructor() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event.value);
    this.textInput.nativeElement.value = '';
  }
}
