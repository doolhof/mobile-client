import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

/**
 * Generated class for the ChatMessageContentAreaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-message-content-area',
  templateUrl: 'chat-message-content-area.html'
})
export class ChatMessageContentAreaComponent implements  OnInit, OnChanges{

  @Input() newMessage = '';
  oldText = '';
  constructor() {}

  ngOnInit(): void {
    if (this.newMessage.length > 1) {
      this.oldText += `<br></br>${this.newMessage}`;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.newMessage.length > 1) {
      this.oldText += `<br></br>${this.newMessage}`;
    }
  }

}
