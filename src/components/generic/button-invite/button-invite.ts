import {Component, Input} from '@angular/core';

/**
 * Generated class for the ButtonInviteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'button-invite',
  templateUrl: 'button-invite.html'
})
export class ButtonInviteComponent {

  @Input() label: string;

  constructor() {

  }

}
