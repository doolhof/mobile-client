import {NgModule} from '@angular/core';
import {GameboardComponent} from './unique/game/gameboard/gameboard';
import {TileComponent} from './unique/game/gameboard/tile/tile';
import {TreasureComponent} from './unique/game/gameboard/treasure/treasure';
import {GamepartyComponent} from './unique/gameparty/gameparty';
import {HostPartySettingComponent} from './unique/gameparty/host-party-setting/host-party-setting';
import {OtherPlayersComponent} from './unique/gameparty/other-players/other-players';
import {ListAreaComponent} from './unique/invites/list-area/list-area';
import {ListComponent} from './unique/invites/list-area/list/list';
import {ListItemComponent} from './unique/invites/list-area/list/list-item/list-item';
import {LoginContentAreaComponent} from './unique/login-content-area/login-content-area';
import {ButtonRegisterSigninComponent} from './unique/login-content-area/button-register-signin/button-register-signin';
import {InputboxLoginComponent} from './unique/login-content-area/inputbox-login/inputbox-login';
import {MainmenuButtonComponent} from './unique/mainmenu-button/mainmenu-button';
import {ButtonDefaultComponent} from './generic/button-default/button-default';
import {ButtonInviteComponent} from './generic/button-invite/button-invite';
import {ChatwindowComponent} from './generic/chatwindow/chatwindow';
import {ChatInputContentComponent} from './generic/chatwindow/chat-input-content/chat-input-content';
import {ChatInputboxComponent} from './generic/chatwindow/chat-input-content/chat-inputbox/chat-inputbox';
import {ChatMessageContentAreaComponent} from './generic/chatwindow/chat-message-content-area/chat-message-content-area';
import {LabelPersonCountComponent} from './generic/label-person-count/label-person-count';
import {NextTurnButtonComponent} from './generic/next-turn-button/next-turn-button';
import {SearchbarComponent} from './generic/searchbar/searchbar';
import {IonicModule} from "ionic-angular";
import {InputboxLoginEncryptedComponent} from './unique/login-content-area/inputbox-login-encrypted/inputbox-login-encrypted';
import {UnfinishedGamesComponent} from './unique/unfinished-games/unfinished-games';
import {UnfinishedItemComponent} from './unique/unfinished-games/unfinished-item/unfinished-item';
import {GamesettingsComponent} from "./unique/gamesettings/gamesettings";
import {PawnComponent} from './unique/game/gameboard/pawn/pawn';
import {MatIconModule} from "@angular/material";
import {InvitesComponent} from './unique/invites/invites';
import {HostProfileComponent} from './unique/gameparty/host-profile/host-profile';
import {OtherPlayerItemComponent} from './unique/gameparty/other-players/other-player-item/other-player-item';
import {LottieAnimationViewModule} from 'lottie-angular2';
import {DeleteAccountComponent} from './unique/delete-account/delete-account';
import {EditPasswordComponent} from './unique/edit-password/edit-password';
import {AddFriendMenuComponent} from './unique/add-friend-menu/add-friend-menu';
import {FriendItemComponent} from './unique/friend-item/friend-item';
import {MatchItemComponent} from './unique/add-friend-menu/match-item/match-item';
import {GameMenuComponent} from './unique/game/game-menu/game-menu';
import {UserPlayerPanelComponent} from './unique/game/user-player-panel/user-player-panel';
import {OtherPlayerPanelComponent} from './unique/game/other-player-panel/other-player-panel';
import {RemainingTileComponent} from './unique/game/remaining-tile/remaining-tile';
import {TurnStatusComponent} from './unique/game/turn-status/turn-status';
import {GameChatComponent} from './unique/game/game-chat/game-chat';
import {GameChatMessageContentComponent} from './unique/game/game-chat/game-chat-message-content/game-chat-message-content';
import {GameChatInputContentComponent} from './unique/game/game-chat/game-chat-input-content/game-chat-input-content';
import {GameChatInputboxComponent} from './unique/game/game-chat/game-chat-input-content/game-chat-inputbox/game-chat-inputbox';

import { NotificationListComponent } from './unique/notification-list/notification-list';
import { NotificationItemComponent } from './unique/notification-list/notification-item/notification-item';
@NgModule({
  declarations: [GameboardComponent,
    TileComponent,
    TreasureComponent,
    GamepartyComponent,
    HostPartySettingComponent,
    OtherPlayersComponent,
    GamesettingsComponent,
    ListAreaComponent,
    ListComponent,
    ListItemComponent,
    LoginContentAreaComponent,
    ButtonRegisterSigninComponent,
    InputboxLoginComponent,
    MainmenuButtonComponent,
    ButtonDefaultComponent,
    ButtonInviteComponent,
    ChatwindowComponent,
    ChatInputContentComponent,
    ChatInputboxComponent,
    ChatMessageContentAreaComponent,
     LabelPersonCountComponent,
    NextTurnButtonComponent,
    SearchbarComponent,
    InputboxLoginEncryptedComponent,
    GamesettingsComponent,
    UnfinishedGamesComponent,
    UnfinishedItemComponent,
    PawnComponent,
    InvitesComponent,
    HostProfileComponent,
    OtherPlayerItemComponent,
    DeleteAccountComponent,
    EditPasswordComponent,
    AddFriendMenuComponent,
    FriendItemComponent,
    MatchItemComponent,
    GameMenuComponent,
    UserPlayerPanelComponent,
    OtherPlayerPanelComponent,
    RemainingTileComponent,
    TurnStatusComponent,
    GameChatComponent,
    GameChatMessageContentComponent,
    GameChatInputContentComponent,
    GameChatInputboxComponent,
    NotificationListComponent,
    NotificationItemComponent,
  ],
  imports: [IonicModule, MatIconModule, LottieAnimationViewModule.forRoot()],

  exports: [GameboardComponent,
    TileComponent,
    TreasureComponent,
    GamepartyComponent,
    HostPartySettingComponent,
    OtherPlayersComponent,
    GamesettingsComponent,
    ListAreaComponent,
    ListComponent,
    ListItemComponent,
    LoginContentAreaComponent,
    ButtonRegisterSigninComponent,
    InputboxLoginComponent,
    MainmenuButtonComponent,
    ButtonDefaultComponent,
    ButtonInviteComponent,
    ChatwindowComponent,
    ChatInputContentComponent,
    ChatInputboxComponent,
    ChatMessageContentAreaComponent,
    LabelPersonCountComponent,
    NextTurnButtonComponent,
    SearchbarComponent,
    InputboxLoginEncryptedComponent,
    GamesettingsComponent,
    UnfinishedGamesComponent,
    UnfinishedItemComponent,
    PawnComponent,
    InvitesComponent,
    HostProfileComponent,
    OtherPlayerItemComponent,
    DeleteAccountComponent,
    EditPasswordComponent,
    AddFriendMenuComponent,
    FriendItemComponent,
    MatchItemComponent,
    GameMenuComponent,
    UserPlayerPanelComponent,
    OtherPlayerPanelComponent,
    RemainingTileComponent,
    TurnStatusComponent,
    GameChatComponent,
    GameChatMessageContentComponent,
    GameChatInputContentComponent,
    GameChatInputboxComponent,
    NotificationItemComponent]
})
export class ComponentsModule {
}
