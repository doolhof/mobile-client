import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Turn} from "../../../../model/game";


@Component({
  selector: 'turn-status',
  templateUrl: 'turn-status.html'
})
export class TurnStatusComponent implements OnChanges {

  timeLeft;
  interval;
  timeIsUp: boolean;
  @Output() stopTurn = new EventEmitter();
  @Input() btnShow;
  @Input() turn: Turn;

  constructor() {

  }

  // TODO set timeLeft in beginning to timePerTurn of GameSettings
  ngOnChanges(changes: SimpleChanges): void {
    clearInterval(this.interval);
    this.startTimer();
  }

  startTimer() {
    this.timeIsUp = false;
    this.timeLeft = this.turn.turnDuration;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeIsUp = true;
        this.onTurnDone();
      }
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.interval);
  }

  onTurnDone() {
    this.stopTimer();
    this.stopTurn.emit(this.timeLeft);
  }


}
