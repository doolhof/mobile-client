import {Component, Input, OnInit} from '@angular/core';
import {ChatSocketMessage, User} from "../../../../model/user";
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {NavController, NavParams} from "ionic-angular";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'game-chat',
  templateUrl: 'game-chat.html'
})
export class GameChatComponent implements OnInit{

  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "https://doolhof-backend.herokuapp.com/";

  @Input() gameId: number;
  private chatStompClient;
  messageText = '';
  private user: User;
  private chatSocketMessage: ChatSocketMessage;
  private responseMessage: any;
  private lobbyId: number;

  constructor(private navCtrl: NavController, private navParams: NavParams, private storage: Storage) {
    this.lobbyId = this.navParams.get('lobbyId');
    console.log(this.lobbyId + "HOWDIEEEEEEE PARTNER")
  }


  ngOnInit(): void {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)});
    this.chatConnect();
  }

  chatConnect() {
    const that = this;
    const socket = new SockJS(this.url + 'chat-socket');
    this.chatStompClient = Stomp.over(socket);
    this.chatStompClient.connect({}, function () {
      that.chatStompClient.send('/app/chat.addUser',
        {},
        JSON.stringify({sender: that.user.username, type: 'JOIN', lobbyId: that.lobbyId}));
      that.chatStompClient.subscribe('/topic/public', (response) => {
        that.onMessageReceived(response);
      });
    });
  }

  onMessageReceived(payload) {
    this.responseMessage = JSON.parse(payload.body);
    console.log(this.lobbyId);
    console.log(this.responseMessage.lobbyId);
    if (this.responseMessage.lobbyId.toString() === this.lobbyId.toString()) {
      if (this.responseMessage.type.toString() === 'JOIN') {
        if (this.responseMessage.content !== undefined) {
          this.responseMessage.content = this.responseMessage.sender + ' joined!';
          this.messageText = this.responseMessage.content;
        }
      } else if (this.responseMessage.type.toString() === 'LEAVE') {
        console.log('LEAVE');
        this.responseMessage.content = this.responseMessage.sender + ' left!';
        this.messageText = this.responseMessage.content;
      } else {
        this.messageText = this.responseMessage.sender + ': ' + this.responseMessage.content;
      }
    }
  }

  sendMessage($event) {
    const chatMessage = {
      sender: this.user.username,
      content: $event.toString(),
      type: 'CHAT',
      lobbyId: this.lobbyId.toString(),
      gameId: '0'
    };
    console.log(JSON.stringify(chatMessage));
    this.chatStompClient.send('/app/chat.sendMessage', {}, JSON.stringify(chatMessage));
  }
}



