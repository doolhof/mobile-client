import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'game-chat-input-content',
  templateUrl: 'game-chat-input-content.html'
})
export class GameChatInputContentComponent {

  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
  }


}
