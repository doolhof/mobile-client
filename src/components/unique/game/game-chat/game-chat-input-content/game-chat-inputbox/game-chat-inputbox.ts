import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';

/**
 * Generated class for the GameChatInputboxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-chat-inputbox',
  templateUrl: 'game-chat-inputbox.html'
})
export class GameChatInputboxComponent {
  @ViewChild('textArea') textInput: ElementRef;
  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
    this.textInput.nativeElement.value = '';
  }
}
