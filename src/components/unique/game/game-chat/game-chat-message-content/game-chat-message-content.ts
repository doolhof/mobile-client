import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

/**
 * Generated class for the GameChatMessageContentComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-chat-message-content',
  templateUrl: 'game-chat-message-content.html'
})
export class GameChatMessageContentComponent implements OnInit, OnChanges {

  @Input() newMessage = '';
  oldText = '';

  constructor() {
  }

  ngOnInit() {
    if (this.newMessage.length > 1) {
      this.oldText += `<br>${this.newMessage}`;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.newMessage.length > 1) {
      this.oldText += `<br>${this.newMessage}`;
    }
  }
}
