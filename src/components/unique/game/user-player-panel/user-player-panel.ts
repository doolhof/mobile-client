import {Component, Input, OnInit} from '@angular/core';
import {Player} from "../../../../model/game";


@Component({
  selector: 'user-player-panel',
  templateUrl: 'user-player-panel.html'
})
export class UserPlayerPanelComponent implements OnInit{
  @Input() player: Player;

  constructor() {
  }

  ngOnInit(): void {
  }

}
