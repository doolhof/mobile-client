import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {MazeCard, Player, Tile, Treasure} from "../../../../../model/game";
import {state, style, trigger} from "@angular/animations";
import {PlayerProvider} from "../../../../../providers/player/player";

@Component({
  selector: 'tile',
  templateUrl: 'tile.html',
  animations: [
    trigger('rotatedState', [
      state('0', style({transform: 'rotate(0)'})),
      state('90', style({transform: 'rotate(90deg)'})),
      state('180', style({transform: 'rotate(180deg)'})),
      state('270', style({transform: 'rotate(270deg)'})),
    ])
  ]
})

export class TileComponent implements  OnInit{
  @Input() tile: Tile;
  @Input() players: Player[];
  @Input() myPlayer: Player;
  @Input() otherPlayers: Player[];
  @Input() steps: String[];
  @Input() gameId: number;
  rotationState: string;
  mazeCardSource: string;
  @Input() mazeCard: MazeCard = {
    mazeCardId: 2,
    orientation: 90,
    northWall: false,
    eastWall: false,
    southWall: false,
    westWall: true,
    imageType: 2,
    treasure: new Treasure(1, 'earth')
  };

  constructor(private  playerService: PlayerProvider) {
  }

  ngOnInit() {
    this.rotationState = this.mazeCard.orientation.toString();
    this.mazeCardSource = '../../../../../assets/mazecards/' + this.mazeCard.imageType + '_alt2.png';
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.playerService.getAllPlayers(this.gameId).subscribe(res => {
      this.players = res;
    });
  }

}
