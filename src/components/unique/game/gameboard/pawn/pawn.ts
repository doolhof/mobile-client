import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';


export let x = 0;
export let y = 0;

@Component({
  selector: 'pawn',
  templateUrl: 'pawn.html'
})
export class PawnComponent implements OnInit, OnChanges{

  @Input() color: string;
  @Input() steps: string[];
  state = 'none';
  counter = 0;
  imageSource: string;

  constructor() {
  }

  ngOnInit() {
    this.imageSource = '../../../../../assets/players/idle_' + this.color + '.gif';

  }

  animationStart(event) {
    if (this.state === 'left') {
      x = x - 40;
    } else if (this.state === 'right') {
      x = x + 40;
    } else if (this.state === 'up') {
      y = y - 40;
    } else if (this.state === 'down') {
      y = y + 40;
    }
    console.log('started');
    console.log(x + '//' + y);
  }

  animationEnd(event) {
    console.log(event);
    this.state = this.steps[this.counter];
    this.counter++;
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

}
