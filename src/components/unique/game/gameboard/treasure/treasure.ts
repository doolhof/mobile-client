import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'treasure',
  templateUrl: 'treasure.html'
})
export class TreasureComponent implements OnInit{

  @Input() treasure: string;
  imageSource: string;


  constructor() {

  }

  ngOnInit(): void {
    this.imageSource = '../../../../../assets/treasures/' + this.treasure + '.svg';
  }

}
