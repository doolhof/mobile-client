import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {GameProvider} from "../../../../providers/game/game";
import {
  Game,
  GameBoard,
  MazeCard,
  Player,
  PlayerSocketMessage,
  Tile,
  TileSocketMessage,
  Turn
} from "../../../../model/game";
import {PlayerProvider} from "../../../../providers/player/player";
import {NavController, NavParams} from "ionic-angular";
import {DomSanitizer} from "@angular/platform-browser";
import {MatIconRegistry} from "@angular/material";
import {TokenStorage} from "../../../../app/core/token.storage";
import * as GameBoardUtil from '../../../../util/gameBoardUtil';

@Component({
  selector: 'gameboard',
  templateUrl: 'gameboard.html'
})
export class GameboardComponent implements OnInit {
  @Input() game: Game;
  @Input() myPlayer: Player;
  @Input() otherPlayers: Player[];
  @Input() gameStompClient;
  pathTiles: Tile[];
  lockedArrow: number;
  steps: string[];
  @Input() playerTurn: Player;
  @Output() updatePlayerTurn = new EventEmitter();
  turn = new Turn(10, 'CARDMOVE', new Date(), [], 0, 0, -90);
  infoMsg: string;

  constructor(private _gameservice: GameProvider,
              private tokenstorage: TokenStorage,
              private playerService: PlayerProvider,
              private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('arrowDown', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/arrows/arrowDown.svg'));
    iconRegistry.addSvgIcon('arrowUp', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/arrows/arrowUp.svg'));
    iconRegistry.addSvgIcon('arrowLeft', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/arrows/arrowLeft.svg'));
    iconRegistry.addSvgIcon('arrowRight', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/arrows/arrowRight.svg'));
  }

  public sortByPosition() {
    this.game.gameBoard.tiles.sort((a, b) => b.position - a.position).reverse();
  }

  ngOnInit() {
    this.sortByPosition();
    this.gameStompClient.subscribe('/game/movePawn', res => {
      this.pathTiles = JSON.parse(res.body);
      this.pathTiles.forEach(obj => {
        this.turn.playerMoves.push(obj.position);
      });
      this.gameStompClient.send('/gameParty/getPlayer', {}, JSON.stringify(this.myPlayer.playerId));
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.turn.turnPhase === 'CARDMOVE') {
      this.infoMsg = 'place the mazecard.';
    } else {
      this.infoMsg = 'move your player.';
    }
  }


  /**This method searches for the arrow button that must be locked (opposite side)*/
  lockArrow(number: any) {
    this.lockedArrow = GameBoardUtil.getDisabledArrow(number);
  }


  /**Method for moving a tile on the gameBoard*/
  moveTile(number: any) {
    if (this.turn.turnPhase !== 'PLAYERMOVE') {
      this.turn.posMazeCardMove = number;
      this.turn.turnPhase = 'PLAYERMOVE';
      this.lockArrow(number);
      const tileSocketMessage = new TileSocketMessage(number.toString(), this.game.gameBoard.gameBoardId.toString());
      this.gameStompClient.send('/gameParty/moveMazeCard', {}, JSON.stringify(tileSocketMessage));
      const playerSocketMessage = new PlayerSocketMessage(this.myPlayer.playerId.toString(), number.toString());
      this.gameStompClient.send('/gameParty/movePlayerTile', {}, JSON.stringify(playerSocketMessage));
    }
  }

  movePawn(destination: number) {
    if (this.turn.turnPhase !== 'CARDMOVE') {
      const playerSocketMessage = new PlayerSocketMessage(this.myPlayer.playerId.toString(), destination.toString());
      this.gameStompClient.send('/gameParty/move', {}, JSON.stringify(playerSocketMessage));
      this.gameStompClient.send('/gameParty/treasureSearch', {}, JSON.stringify(playerSocketMessage));
      this.updatePlayerTurn.emit(this.turn);
    }
  }

  updateTurn() {
    this.turn = new Turn(10, 'CARDMOVE', new Date(), [], 0, 0, -90);
  }
}
