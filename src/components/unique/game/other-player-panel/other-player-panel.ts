import {Component, Input} from '@angular/core';
import {Player} from "../../../../model/game";

@Component({
  selector: 'other-player-panel',
  templateUrl: 'other-player-panel.html'
})
export class OtherPlayerPanelComponent {

  @Input() player: Player;

  constructor() {
    // this.player = new Player(1000, 'BLUE', 'Player 420', [new Treasure('earth'), new Treasure('monster_1')],
    // [new Treasure('mars'), new Treasure('planet_1'), new Treasure('planet_2'),
    //   new Treasure('mars'), new Treasure('planet_1'), new Treasure('mars')], null, 40, 40, false);
  }

}
