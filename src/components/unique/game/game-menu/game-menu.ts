import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavController} from "ionic-angular";
import {MainmenuPage} from "../../../../pages/mainmenu/mainmenu";

/**
 * Generated class for the GameMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'game-menu',
  templateUrl: 'game-menu.html'
})
export class GameMenuComponent implements  OnInit{
  @Output() back = new EventEmitter();

  constructor(private navCtrl: NavController) {

  }

  ngOnInit() {
  }

  onMainMenu() {
    this.navCtrl.push(MainmenuPage);
  }

  onBack() {
    this.back.emit();
  }

}
