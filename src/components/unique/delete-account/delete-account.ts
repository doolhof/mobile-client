import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from "../../../model/user";
import {UserProvider} from "../../../providers/user/user";
import {NavController} from "ionic-angular";
import {LoginPage} from "../../../pages/login/login";

/**
 * Generated class for the DeleteAccountComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'delete-account',
  templateUrl: 'delete-account.html'
})
export class DeleteAccountComponent implements  OnInit{

  @Output() back = new EventEmitter();
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));

  constructor(private  userService: UserProvider, private navCtrl: NavController) {

  }

  ngOnInit() {
  }
  onBack() {
    this.back.emit();
  }
  onYes() {
    this.userService.deleteAccount(this.user.userId).subscribe(res => {
    });
    this.userService.logout();
    this.navCtrl.push(LoginPage);
    this.back.emit();
  }
}
