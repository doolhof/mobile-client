import {Component, Input} from '@angular/core';

/**
 * Generated class for the HostPartySettingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'host-party-setting',
  templateUrl: 'host-party-setting.html'
})
export class HostPartySettingComponent {
  @Input() online: number;
  constructor() {
  }

}
