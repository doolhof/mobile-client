import {Component, Input} from '@angular/core';
import {User} from "../../../../model/user";

/**
 * Generated class for the HostProfileComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'host-profile',
  templateUrl: 'host-profile.html'
})
export class HostProfileComponent {
  @Input() hostUser: User;
  constructor() {}
}
