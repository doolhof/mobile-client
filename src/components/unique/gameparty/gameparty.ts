import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from "../../../model/user";

/**
 * Generated class for the GamepartyComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'gameparty',
  templateUrl: 'gameparty.html'
})
export class GamepartyComponent implements  OnInit, OnChanges{
  @Input() users: User[];
  @Input() hostUser: User;
  private otherUsers: User[];

  constructor() {}

  ngOnInit(): void {
    this.otherUsers = this.users.filter(user => user.userId !== this.hostUser.userId);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['users'] && changes['users'].currentValue !== changes['users'].previousValue) {
      this.otherUsers = this.users.filter(user => user.userId !== this.hostUser.userId);
    }
  }

}
