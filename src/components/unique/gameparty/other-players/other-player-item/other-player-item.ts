import {Component, Input} from '@angular/core';
import {User} from "../../../../../model/user";

/**
 * Generated class for the OtherPlayerItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'other-player-item',
  templateUrl: 'other-player-item.html'
})
export class OtherPlayerItemComponent {
  @Input() user: User;
  constructor() {}

}
