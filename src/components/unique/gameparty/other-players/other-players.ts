import {Component, Input} from '@angular/core';
import {User} from "../../../../model/user";

/**
 * Generated class for the OtherPlayersComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'other-players',
  templateUrl: 'other-players.html'
})
export class OtherPlayersComponent {
  @Input() otherUsers: User[];

  constructor() {
  }

}
