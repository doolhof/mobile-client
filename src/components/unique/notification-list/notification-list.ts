import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the NotificationListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notification-list',
  templateUrl: 'notification-list.html'
})
export class NotificationListComponent {
  @Output() close = new EventEmitter();
  @Input() notifications;


  constructor() {
  }

  onCloseIt($event) {
    this.close.emit();
  }
}
