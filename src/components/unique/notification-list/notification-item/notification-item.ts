import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NotificationProvider} from "../../../../providers/notification/notification";

/**
 * Generated class for the NotificationItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notification-item',
  templateUrl: 'notification-item.html'
})
export class NotificationItemComponent {
  @Input() notification;
  @Output() closeNotif = new EventEmitter();


  constructor(private notificationProvider: NotificationProvider) {
  }
  goToLobby() {
    this.closeNotif.emit();


  }

  closeNotification() {

    this.closeNotif.emit();
    this.notificationProvider.putNotificationToClose(this.notification.notificationId).subscribe(e => console.log(e));
  }
}
