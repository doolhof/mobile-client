import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the MainmenuButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mainmenu-button',
  templateUrl: 'mainmenu-button.html'
})
export class MainmenuButtonComponent {
  @Input() label: string;

  constructor() {

  }
}
