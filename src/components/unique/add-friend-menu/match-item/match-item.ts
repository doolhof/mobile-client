import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../../model/user";

/**
 * Generated class for the MatchItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'match-item',
  templateUrl: 'match-item.html'
})
export class MatchItemComponent implements OnInit {
@Input() friend: User;
@Input() selected: boolean;

  constructor() {

  }

  ngOnInit(): void {
  }

}
