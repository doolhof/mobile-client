import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../model/user";
import {UserProvider} from "../../../providers/user/user";
import {FriendsProvider} from "../../../providers/friends/friends";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'add-friend-menu',
  templateUrl: 'add-friend-menu.html'
})
export class AddFriendMenuComponent {
  @Input() user: User;
  @Output() back = new EventEmitter();
  @Output() updateUser = new EventEmitter();
  matchingUser: User;
  selectedUser: User;
  hasSearched: boolean;

  constructor(private userService: UserProvider, private friendService: FriendsProvider, private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)
      this.userService.getUserByUserId(this.user.userId).subscribe(res => this.user = res);
    });


  }

  ngOnInit() {
    this.hasSearched = false;
  }

  onSelectMatch(user: User) {
    this.selectedUser = user;
  }

  onUnselect() {
    this.selectedUser = null;
  }

  onBack() {
    this.back.emit();
  }

  onAddFriend(friend: User) {
    this.friendService.addFriend(this.user.userId, friend.userId).subscribe(res => {
      this.updateUser.emit();
      this.matchingUser = null;
      this.selectedUser = null;
    });
    }

  search(event): void {
    this.matchingUser = null;
    this.userService.getUserByUsername(event).subscribe(res => {
      if (!this.user.friends.find(f => f.username === res.username)) {
        this.matchingUser = res;
      }
    });
    this.userService.getUserByUserId(this.user.userId).subscribe(res => this.user = res);
    this.hasSearched = true;
  }
}
