import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'friend-item',
  templateUrl: 'friend-item.html'
})
export class FriendItemComponent implements  OnInit{
  @Input() friend: User;
  @Input() selected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
