import {Component, Input} from '@angular/core';
import {Lobby, User} from "../../../../model/user";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'unfinished-item',
  templateUrl: 'unfinished-item.html'
})
export class UnfinishedItemComponent {

  @Input() public Lobby: Lobby;
  @Input() public selected: boolean;
  inlogUser: User;

  constructor(private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user)
    });
  }

  ngOnInit() {
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user)
    });
  }

}
