import {Component, EventEmitter, Output} from '@angular/core';
import {LobbyProvider} from "../../../providers/lobby/lobby";
import {Lobby, User} from "../../../model/user";
import {NavController} from "ionic-angular";
import {LobbyPage} from "../../../pages/lobby/lobby";
import {Storage} from '@ionic/storage';
import {MainmenuPage} from "../../../pages/mainmenu/mainmenu";
/**
 * Generated class for the UnfinishedGamesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'unfinished-games',
  templateUrl: 'unfinished-games.html'
})
export class UnfinishedGamesComponent {
  @Output() backToMenu = new EventEmitter();
  selectedLobby: Lobby;
  inlogUser: User;
  lobbies: Lobby[];

  constructor(private lobbyService: LobbyProvider, private navCtrl: NavController, private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user)
      this.lobbyService.getLobbiesUser(this.inlogUser.username).subscribe(res => {
        this.lobbies = res
      });
    });
  }

  ngOnInit() {
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user)
      this.lobbyService.getLobbiesUser(this.inlogUser.username).subscribe(res => {
        this.lobbies = res;});

    });
  }

  toMenu() {
    this.navCtrl.push(MainmenuPage);
    this.backToMenu.emit(false);
     }
  loadLobby() {
   this.navCtrl.push(LobbyPage, this.selectedLobby.lobbyId);
  }

  onSelect(lobby: Lobby): void {
    this.selectedLobby = lobby;
  }

  }
