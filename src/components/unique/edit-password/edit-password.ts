import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PasswordDto, User} from "../../../model/user";
import {UserProvider} from "../../../providers/user/user";

@Component({
  selector: 'edit-password',
  templateUrl: 'edit-password.html'
})
export class EditPasswordComponent implements OnInit{

  @Output() back = new EventEmitter();
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));
  oldPassword: string;
  newPassword: string;
  passwordDto: PasswordDto;
  constructor(private  userService: UserProvider) {}

  ngOnInit() {
  }
  onBack() {
    this.back.emit();
  }
  readOldPassword(password): void {
    this.oldPassword = password;
  }
  readNewPassword(password): void {
    this.newPassword = password;
  }
  onConfirm() {
    this.passwordDto = new PasswordDto(this.user.userId, this.oldPassword, this.newPassword);
    this.userService.updatePassword(new PasswordDto(this.user.userId, this.oldPassword, this.newPassword))
      .subscribe(res => {
      });
    this.back.emit();
  }
}
