import {Component, EventEmitter, Output} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {User} from "../../../../model/user";

/**
 * Generated class for the InputboxLoginEncryptedComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'inputbox-login-encrypted',
  templateUrl: 'inputbox-login-encrypted.html'
})
export class InputboxLoginEncryptedComponent{
  placeholderInput: string;
  @Output() signInfo = new EventEmitter<User>();
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor() {
  }



  sendSignInfo(info) {
    this.signInfo.emit(info);
  }
}
