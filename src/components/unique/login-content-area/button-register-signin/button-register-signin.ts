import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the ButtonRegisterSigninComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'button-register-signin',
  templateUrl: 'button-register-signin.html'
})
export class ButtonRegisterSigninComponent {

  @Input() label: string;
  @Output() btnClick = new EventEmitter();

  constructor() {

  }

}
