import {Component, OnInit} from '@angular/core';
import {User} from "../../../model/user";
import {TokenStorage} from "../../../app/core/token.storage";
import {NavController, NavParams} from "ionic-angular";
import {HttpErrorResponse} from "@angular/common/http";
import {UserProvider} from "../../../providers/user/user";
import {MainmenuPage} from "../../../pages/mainmenu/mainmenu";
import {RegistrationPage} from "../../../pages/registration/registration";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'login-content-area',
  templateUrl: 'login-content-area.html'
})
export class LoginContentAreaComponent implements OnInit {

  user: User;
  email: string;
  password: string;
  invalidEmail: string;
  invalidPassword: string;
  username: string;
  authenticated: Boolean = false;
  constructor(public navCtrl: NavController, public navParam: NavParams,
              private tokenstorage: TokenStorage,
              private userProvider: UserProvider,
              private storage: Storage ) {
  }

  ngOnInit() {
    this.tokenstorage.signOut();
  }

  signInWithEmail(): void {
    this.userProvider.postLogin(this.username, this.password).subscribe(res => {
      if (res.token !== null) {
        this.authenticated = true;
        this.tokenstorage.saveToken(res.token);
      }
         if (this.authenticated) {
        this.userProvider.getUserByUsername(this.username).subscribe(res1 => {
          if (!(res1 instanceof HttpErrorResponse) && res1 !== null) {
            this.storage.set('currentUser', JSON.stringify(res1));
            this.redirectToMainMenu();
          }
        });
      }
    }, error => {
      this.invalidEmail = 'wrong username';
      this.invalidPassword = '';
    });
  }

  redirectToMainMenu(): void {
    this.navCtrl.push(MainmenuPage);
  }

  readUsername(username): void {
    this.username = username;
  }

  readPassword(password): void {
    this.password = password;
  }

  register(): void {
    this.navCtrl.push(RegistrationPage);
  }

}
