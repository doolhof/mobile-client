import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {User} from "../../../../model/user";

/**
 * Generated class for the InputboxLoginComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'inputbox-login',
  templateUrl: 'inputbox-login.html'
})
export class InputboxLoginComponent {

  @Input() placeholderInput: string;
  @Output() signInfo = new EventEmitter<User>();
  usernameFormControl = new FormControl('', [
    Validators.required,
  ]);


  constructor() {
  }

  ngOnInit() {
  }

  sendSignInfo(info) {
    this.signInfo.emit(info);
  }

}
