import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from "../../../model/user";
import {SelectableListItemDomain} from "./list-area/list/list-item/selectableListItemDomain";
import {LobbyProvider} from "../../../providers/lobby/lobby";
import {Storage} from '@ionic/storage';
import {FriendsProvider} from "../../../providers/friends/friends";

@Component({
  selector: 'invites',
  templateUrl: 'invites.html'
})
export class InvitesComponent implements OnInit, OnChanges {

  @Input() lobbyId: number;
  community: User[];
  friends: User[];
  private user: User;
  private tabIndex: number;
  private maxListCounter = 2;
  private listSelectableItem: SelectableListItemDomain[];



  constructor(private lobbyProvider: LobbyProvider, private storage: Storage, private friendProvider: FriendsProvider) {

  }

  ngOnInit() {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
      this.lobbyProvider.getCommunity(this.user.userId, 0, this.maxListCounter).subscribe(res => {
        this.community = res;
      });
      this.friendProvider.getFriendsByUser(this.user.userId).subscribe(f => this.friends = f);
    });
    this.tabIndex = 2;
  }

  setSelectableItemsList($event) {
    this.listSelectableItem = $event;
  }

  changeTab(tabNr) {
    this.tabIndex = tabNr;
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

}
