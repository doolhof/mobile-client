import {Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';
import {User} from "../../../../../model/user";
import {SelectableListItemDomain} from "./list-item/selectableListItemDomain";
import {LobbyProvider} from "../../../../../providers/lobby/lobby";

import {Storage} from '@ionic/storage';

@Component({
  selector: 'list',
  templateUrl: 'list.html'
})
export class ListComponent {
  @Input() searchResults: string;
  @Input() lobbyId: number;
  @Input() selectableItems: SelectableListItemDomain[];
  @Input() isRadioButtonList: boolean;
  @Input() tabIndex: number;
  @Output() cellClick = new EventEmitter();
  @Output() selectableItemsEmitter = new EventEmitter();
  private longerPage: SelectableListItemDomain[];
  private maxListCounter = 1;
  private user: User;

  constructor(private lobbyprovider: LobbyProvider, private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.user = user;
    });
  }


  selectableItemClick = (index: number) => {
    if (this.isRadioButtonList) {
      this.clearSelected();
      return this.selectableItems[index].isSelected = true;
    }
    this.selectableItems[index].isSelected = !this.selectableItems[index].isSelected;
  };

  private clearSelected = () => {
    for (let i = 0; i < this.selectableItems.length; i++) {
      this.selectableItems[i].isSelected = false;
    }
  };

  sendInvitations() {
    this.lobbyprovider.sendInvitation(this.selectableItems.filter(item => item.isSelected)
      .map(item => item.user.userId.toString()), this.lobbyId).subscribe(res => {
    });
  }


  loadMoreUsers() {
    this.maxListCounter += 3;
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
      this.lobbyprovider.getCommunity(this.user.userId, 0, this.maxListCounter).subscribe(res => {
        if (res.length >= 1) {
          res.forEach(user => {
            let isPresent = this.selectableItems.map(item => item.user.userId).some(function (el) {
              return el === user.userId
            });
            if (!isPresent) {
              this.selectableItems.push(new SelectableListItemDomain(user, false));
            }
          });
          this.selectableItemsEmitter.emit(this.selectableItems);
        }
      });
    });
  }



  ngOnChanges(changes: SimpleChanges): void {

  }

}
