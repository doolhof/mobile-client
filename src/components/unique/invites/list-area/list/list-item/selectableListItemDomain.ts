import {User} from "../../../../../../model/user";

export class SelectableListItemDomain {

    constructor(public user: User, public isSelected: Boolean = false) {
    }
}
