import {Component, Input} from '@angular/core';
import {SelectableListItemDomain} from "./selectableListItemDomain";

/**
 * Generated class for the ListItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-item',
  templateUrl: 'list-item.html'
})
export class ListItemComponent {


  @Input() set isSelected(object: boolean) {
    this.selected = object;
    this.toggle();
  }

  @Input() selectableItem: SelectableListItemDomain;
  @Input() name: string;
  @Input() index: Number;
  @Input() selectFunction: (number: Number) => {};
  text: string;

  private selected: boolean;
  public lottieConfig: Object;
  private anim: any;

  constructor() {
    this.lottieConfig = {
      path: 'assets/anim/checked-loading3.json',
      autoplay: false,
      loop: false
    };
  }

  animInit(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  celClicked = () => {
    this.selectFunction(this.index);
    this.toggle();
  };

  toggle() {
    if (this.anim) {
      this.selected ? this.anim.play() : this.anim.stop();
    }
  }

  ngOnInit(): void {
    if (this.selectableItem.isSelected) {
      this.anim.play();
    }
  }
}
