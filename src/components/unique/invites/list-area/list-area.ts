import {Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';
import {LobbyProvider} from "../../../../providers/lobby/lobby";
import {SelectableListItemDomain} from "./list/list-item/selectableListItemDomain";
import {User} from "../../../../model/user";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the ListAreaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-area',
  templateUrl: 'list-area.html'
})
export class ListAreaComponent {
  @Input() lobbyId: number;
  @Input() community: User[];
  @Input() tabIndex: number;
  @Input() cachedListSelectableItem: SelectableListItemDomain[];
  @Output() selectableItemsEmitter = new EventEmitter();
  searchResults: string;
  listSelectableItem: SelectableListItemDomain[];
  searchList: SelectableListItemDomain[] = [];
  user: User;


  constructor(private  lobbyProvide: LobbyProvider, private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
    });
  }
  ngOnInit() {
    this.listSelectableItem = this.community.map(user => new SelectableListItemDomain(user, false));
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
    });
  }

  showSearchResults(event) {
    this.searchResults = event;
    this.initLists();
  }

  setSelectableItemsList($event) {
    this.selectableItemsEmitter.emit($event);
  }

  initLists() {
    this.searchList = [];
    if (this.searchResults.length >= 1) {
      this.lobbyProvide.searchUsersByUsername(this.searchResults, this.user.userId).subscribe(res => {
        this.listSelectableItem = [];
        res.forEach(user => {
          let isPresent = this.listSelectableItem.map(item => item.user.userId).some(function(el){ return el === user.userId});
          if (!isPresent) {
            this.listSelectableItem.push(new SelectableListItemDomain(user, false));
          }
        });
      });
    } else if (!this.cachedListSelectableItem) {
      this.listSelectableItem = this.community.map(user => new SelectableListItemDomain(user, false));
    } else {
      this.listSelectableItem = this.cachedListSelectableItem;
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
  }
}
