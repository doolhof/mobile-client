import {Component, EventEmitter, Output} from '@angular/core';
import {User} from "../../../model/user";
import {TokenStorage} from "../../../app/core/token.storage";
import {UserProvider} from "../../../providers/user/user";
import {LobbyProvider} from "../../../providers/lobby/lobby";
import {NavController, NavParams} from "ionic-angular";
import {LobbyPage} from "../../../pages/lobby/lobby";
import {Storage} from '@ionic/storage';


@Component({
  selector: 'gamesettings',
  templateUrl: 'gamesettings.html'
})
export class GamesettingsComponent {

  data: { gameSettingId: number, amountPlayer: number, secondsTurn: number };
  currentUser: User;
  @Output() backToMenu = new EventEmitter();


  constructor(private  userProvider: UserProvider,
              private tokenstorage: TokenStorage,
              private lobbyProvider: LobbyProvider,
              public navCtrl: NavController,
              navParams: NavParams,
              private storage: Storage) {
    this.storage.get('currentUser').then(user => {
      this.currentUser = JSON.parse(user);
    });
    this.data = {
      gameSettingId: 1,
      amountPlayer: 2,
      secondsTurn: 60
    };
  }

  onSubmit() {
    if (this.data.secondsTurn && this.data.amountPlayer) {
      this.lobbyProvider.postCreateLobby(this.currentUser.userId, this.data.secondsTurn, this.data.amountPlayer).subscribe(res => {
             this.navCtrl.push(LobbyPage, {lobbyId: res.lobbyId.toString()});
      });
    }
  }

  toMenu() {
    this.backToMenu.emit(false);
  }
}
