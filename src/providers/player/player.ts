import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Player, Tile} from "../../model/game";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

/*
  Generated class for the PlayerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlayerProvider {

  headers = new HttpHeaders();
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";
  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  movePlayer(playerId: number, destination: number): Observable<Tile[]> {
    return this.http.post<Tile[]>(this.url +`api/player/move?playerId=${playerId}&destination=${destination}`
      , {headers: this.headers}).pipe(map(res => res.map(x =>
      new Tile(x))));
  }

  movePlayerWithTile(playerId: number, destination: number): Observable<Player[]> {
    return this.http.post<Player[]>(this.url +`api/player/movePlayerTile?playerId=${playerId}&destination=${destination}`
      , {headers: this.headers}).pipe(map(res => res.map(x =>
      new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.isTurn))));
  }

  getAllPlayers(gameId: number): Observable<Player[]> {
    return this.http.get<Player[]>(this.url +`api/player/game/${gameId}`, {headers: this.headers}).pipe(
      map(res => res.map(x =>
        new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.isTurn))));
  }

  // searchTreasure(playerId: number): Observable<Player> {
  //   return this.http.post<Player>(`http://localhost:9090/api/player/treasureSearch?playerId=${playerId}`,
  //     {headers: this.headers}).pipe(
  //     map(player => new Player(player.playerId, player.color, player.name, player.toFind, player.found,
  //       player.turns, player.currentPosition, player.startPosition)));
  // }

//   checkWinGame(playerId: number): Observable<boolean> {
//     return this.http.post<Player>(`http://localhost:9090/api/player/checkWinGame?playerId=${playerId}`,
//       {headers: this.headers}).pipe(
//       map(res => Boolean(res)));
// =======
//         new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.isTurn))));
// >>>>>>> master
//   }

}
