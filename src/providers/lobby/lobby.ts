import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Lobby, User} from "../../model/user";
import {Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {of} from "rxjs/observable/of";

@Injectable()
export class LobbyProvider {

  headers = new HttpHeaders();
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";
  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  postCreateLobby(userId: number, secondsTurn: number, amountPlayer: number): Observable<Lobby> {
    return this.http.post<Lobby>(this.url +`api/lobby/createLobby/${userId}/${amountPlayer}/${secondsTurn}`,
      {headers: this.headers})
      .pipe(map(res => new Lobby(res.lobbyId, res.listUsers, res.gameSettings, res.host, res.game)));
  }

  getLobbyById(id: number): Observable<Lobby> {
    return this.http.get<Lobby>(this.url + `api/lobby/${id}`)
      .pipe(map(res => new Lobby(res.lobbyId, res.listUsers, res.gameSettings, res.host, res.game)));
  }

  getLobbiesUser(username: string): Observable<Lobby[]> {
    console.log('in getLobbies');
    return this.http.get<Lobby[]>(this.url +`api/lobby/loadLobbies/${username}`, {headers: this.headers})
      .pipe(map(res => res.map(x =>
          new Lobby(x.lobbyId, x.listUsers, x.gameSettings, x.host, x.game)),
        catchError(err => {
          return of(null);
        })));
  }

  sendInvitation(users: string[], lobbyId: number)  {
    console.log(users.toString());
    return this.http.post(this.url +`api/lobby/invite/${lobbyId}?userIds=${users}`, {headers: this.headers});
  }

  getCommunity(userId: number, min: number, max: number): Observable<User[]> {
    return this.http.get<User[]>(this.url +`api/users/community/${userId}/${min}/${max}`, {headers: this.headers}).pipe(
      map(x => x.map(res =>
        new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
          res.userStatistic, res.userSetting, res.notifications, res.fcmToken))));
  }

  searchUsersByUsername(username: string, userId: number): Observable<User[]> {
    return this.http.get<User[]>(this.url +`api/users/usernames/${username}/${userId}`, {headers: this.headers}).pipe(
      map(x => x.map(res =>
        new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
          res.userStatistic, res.userSetting, res.notifications, res.fcmToken))));
  }
}
