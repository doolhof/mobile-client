import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {FbInfo, PasswordDto, Token, User} from "../../model/user";
import {catchError, map} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import {Storage} from '@ionic/storage';

@Injectable()
export class UserProvider {
  headers = new HttpHeaders();
  user: User[];
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";
  constructor(private http: HttpClient, private storage: Storage) {
    this.headers.append('Content-Type', 'application/json');
  }


  getUserByUserId(id: number): Observable<User> {
    return this.http.get<User>(this.url +'api/users/id/' + id.toString(), {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)));
  }

  getUserByEmail(email: string): Observable<User> {
    return this.http.get<User>(this.url +'api/users/email/' + email, {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)));
  }

  getUserByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.url +'api/users/username/' + username, {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken),
        catchError(err => {
          return of(null);
        })));
  }

  postLogin(username: string, password: string): Observable<Token> {
    return this.http.post<Token>(this.url +'api/users/signin?username=' + username + '&password=' + password
      , {headers: this.headers})
      .pipe(map(res => new Token(res.token, res.user)),
        catchError(err => {
          return of(null);
        }));
  }

  postSignUp(user: User): Observable<Token> {
    console.log('in postsignup');
    return this.http.post<Token>(this.url +'api/users/sign-up', user, {headers: this.headers})
      .pipe(map(res => new Token(res.token, res.user)),
        catchError(err => {
          return of(null);
        }));
  }

  postConfirm(token: string): Observable<Boolean> {
    console.log('in postconfirm');
    console.log(token);
    return this.http.post<Boolean>(this.url +'api/users/confirm?token=' + token, {headers: this.headers});
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>(this.url +'api/users/update', user, {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }

  updatePassword(passwordDto: PasswordDto): Observable<Boolean> {
    return this.http.put<Boolean>(this.url +'api/users/updatePassword', passwordDto, {headers: this.headers});
  }

  deleteAccount(userId: number): Observable<Boolean> {
    return this.http.delete<Boolean>(this.url +'api/users/deleteAccount/' + userId, {headers: this.headers});
  }

  createAccountWithFb(fbInfo: FbInfo): Observable<User> {
    console.log('in postsignup');
    return this.http.post<User>(this.url +'api/users/sign-up-fb', fbInfo, {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }

  isLoggedIn(): boolean {
    if (this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)}) != null){
      return true;
    } else
    {
      return false;
    }
  }


  logout() {
    this.storage.remove('currentUser');
  }
  updateFcmToken(user: User): Observable<User> {
    return this.http.post<User>('http://localhost:9090/api/users/updateFcmToken?token=' + user.fcmToken + '&id=' + user.userId,
      {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }


}
