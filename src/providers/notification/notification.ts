import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Notificat} from "../../model/user";
import {catchError, map} from "rxjs/operators";
/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationProvider {
  headers = new HttpHeaders();
  private url = "http://localhost:9090/";
  constructor(public http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }
  getAllNotifications(id: number): Observable<Notificat[]> {
    return this.http.get<Notificat[]>(this.url +`api/notification/${id}`, {headers: this.headers})
      .pipe(map(res => res.map(x =>
        new Notificat(x.notificationId, x.notifDesc, x.user, x.notificationType, x.webUrl, x.mobileUrl, x.closed))));

  }

  putNotificationToClose(id: number) {
    console.log('in closeNotification servcie');
    return this.http.post(`http://localhost:9090/api/notification/close/${id}`, {headers: this.headers})
      .pipe(
        map(res => console.log(res))
      );
  }
}
