import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {AngularFireDatabase} from "@angular/fire/database";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireMessaging} from "@angular/fire/messaging";
import {take} from 'rxjs/operators';
import * as firebase from "firebase";
import {User} from "../../model/user";
import {UserProvider} from "../user/user";
import {Storage} from '@ionic/storage';
import {Message} from "@angular/compiler/src/i18n/i18n_ast";


/*import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';*/


@Injectable()
export class FcmProvider {
  tokenStr: string;
  message;
  inlogUser: User;
  currentMessage = new BehaviorSubject(null);
  notificationList: any[];

  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging,
    private userProvider: UserProvider,
    private storage: Storage) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
        console.log(_messaging);
      }
    );
  }

  ngOnInit() {
    console.log('in ngOnInit');
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user);
      console.log(this.inlogUser);
      const userId = this.inlogUser.userId;
      // const userId = 20;
      this.requestPermission(userId);
      this.receiveMessage();
      this.message = this.currentMessage;
    });


  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token;
        console.log(data);
        this.angularFireDB.object('fcmTokens/').update(data);
       // document.getElementById('consoleLog').innerHTML += "hello " + this.tre;
         this.inlogUser.fcmToken = token;
        this.userProvider.updateFcmToken(this.inlogUser).subscribe(res => {
         this.inlogUser = res;
         console.log(res);
        });
      });
    console.log(this.tokenStr);
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.updateToken(userId, token);
        this.tokenStr = token;
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
    console.log(this.tokenStr);
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log('new message received. ', payload);
        this.currentMessage.next(payload);
      });
  }


}
