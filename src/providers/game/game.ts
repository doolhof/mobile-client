import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Game, GameBoard, MazeCard} from "../../model/game";
import {map} from "rxjs/operators";

/*
  Generated class for the GameProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GameProvider {


  headers = new HttpHeaders();
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";
  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  // GET
  getGameboard(gameboardId: number): Observable<GameBoard> {
    return this.http.get<GameBoard>(this.url +'api/gameboard/' + gameboardId.toString(), {headers: this.headers}).pipe(
      map(res => new GameBoard(res))
    );
  }

  getNewGame(lobbyId: number): Observable<Game> {
    return this.http.get<Game>(this.url + 'api/game/startGame/' + lobbyId.toString(), {headers: this.headers}).pipe(
      map(res => new Game(res))
    );
  }

  getStartGame(lobbyId: number): Observable<Game> {
    return this.http.get<Game>(this.url + 'api/game/startGame/' + lobbyId.toString(), {headers: this.headers}).pipe(
      map(res => new Game(res))
    );
  }

  // POST
  postRemainingMazeCard(mazecard: MazeCard): void {
    this.http
      .post(this.url + 'api/mazecard/updateRemaining', mazecard, {headers: this.headers})
      .toPromise()
      .then(res => res.toString());
  }

  // POST
  postMove(position: Number, gameboardId: Number): Observable<GameBoard> {
    return this.http.post<GameBoard>(this.url + `api/tile/moveTile?gameboardId=${gameboardId}`, position)
      .pipe(map(res => new GameBoard(res)));
  }

  getGame(id: number): Observable<Game> {
    return this.http.get<Game>(this.url + 'api/game/' + id).pipe(
      map(res => new Game(res))
    );
  }

  getGamesUser(username: String): Observable<Game[]> {
    console.log('in getgamesuser');
    return this.http.get<Game[]>(this.url + 'api/game/loadAllGames/' + username)
      .pipe(map(res => res.map(x =>
        new Game(x))));
  }



}
