import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../../model/user";
import {map} from "rxjs/operators";

@Injectable()
export class FriendsProvider {

  headers = new HttpHeaders();
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";
  constructor(private http: HttpClient) {
  }

  getFriendsByUser(userId: number): Observable<User[]> {
    return this.http.get<User[]>(this.url +'api/friends/' + userId).pipe(
      map(res => res.map(x => new User(x.userId, x.username, x.friends, x.email, x.encryptedPassword, x.lobbies, x.userStatistic, x.userSetting, x.notifications, x.fcmToken)))

    );
  }

  addFriend(userId: number, friendId: number): Observable<User> {
    return this.http.put<User>(this.url +'api/friends/add/' + userId.toString() + '/' + friendId.toString(),
      null, {headers: this.headers}).pipe(
      map(res => res = new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))
    );
  }

  removeFriendByUser(userId: number, friendId: number): Observable<User> {
    return this.http.put<User>(this.url +'api/friends/remove/' + userId.toString() + '/' + friendId.toString(),
      null, {headers: this.headers}).pipe(
      map(res =>
        res = new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))
    );
  }
}
