
export function getDisabledArrow(number) {
  if (number === 2 || number === 4 || number === 6) {
    number =  (number + 42);
    return number;
  } else if (number === 14 || number === 28 || number === 42) {
    number = (number - 6);
    return number;
  } else if (number === 8 || number === 22 || number === 36) {
    number = (number + 6);
    return number;
  } else if (number === 44 || number === 46 || number === 48) {
    number = (number - 42);
    return number;
  }
}
