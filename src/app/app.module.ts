import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {MyApp} from './app.component';
import {ScreenOrientation} from "@ionic-native/screen-orientation/ngx";
import {ComponentsModule} from "../components/components.module";
import {LoginPage} from "../pages/login/login";
import {GameProvider} from '../providers/game/game';
import {UserProvider} from '../providers/user/user';
import {HomePage} from "../pages/home/home";
import {TokenStorage} from "./core/token.storage";
import {HttpClientModule} from "@angular/common/http";
import {Interceptor} from "./core/interceptor";
import {ReactiveFormsModule} from "@angular/forms";
import {GametimePage} from "../pages/game/gametime";
import {LobbyProvider } from '../providers/lobby/lobby';
import {PlayerProvider } from '../providers/player/player';
import {MatIconModule} from '@angular/material/icon';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MainmenuPage} from "../pages/mainmenu/mainmenu";
import {LottieAnimationViewModule} from 'lottie-angular2';
import {RegistrationPage} from "../pages/registration/registration";
import {RegistrationSuccesPage} from "../pages/registration/registration-succes/registration-succes";
import {RegistrationConfirmPage} from "../pages/registration/registration-confirm/registration-confirm";
import {LobbyPage} from "../pages/lobby/lobby";
import {IonicStorageModule } from '@ionic/storage';
import {ProfilePage} from "../pages/profile/profile";
import {MatSlideToggleModule} from "@angular/material";
import {FriendsProvider } from '../providers/friends/friends';
import {FriendsPage} from "../pages/friends/friends";
import {ErrorPage} from "../pages/error/error";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFireModule} from "@angular/fire";
import {AngularFireMessagingModule} from "@angular/fire/messaging";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {FcmProvider} from "../providers/fcm/fcm";
import {NotificationPage} from "../pages/notification/notification";
import { NotificationProvider } from '../providers/notification/notification';

const firebase =  {
  apiKey: "AIzaSyD2hHT_qHQe6hfbglnFnlbkq9dvqYZ9iOE",
  authDomain: "mazetrix-c09ac.firebaseapp.com",
  databaseURL: "https://mazetrix-c09ac.firebaseio.com",
  projectId: "mazetrix-c09ac",
  storageBucket: "mazetrix-c09ac.appspot.com",
  messagingSenderId: "250908293848"};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    GametimePage,
    MainmenuPage,
    RegistrationPage,
    RegistrationSuccesPage,
    RegistrationConfirmPage,
    LobbyPage,
    ProfilePage,
    FriendsPage,
    ErrorPage,
    NotificationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    IonicStorageModule.forRoot(),
    LottieAnimationViewModule.forRoot(),
    IonicStorageModule.forRoot(),
   /* AngularFireModule,
    AngularFirestoreModule*/
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(firebase)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage, GametimePage,
    MainmenuPage,
    RegistrationPage,
    RegistrationSuccesPage,
    RegistrationConfirmPage,
    LobbyPage,
    ProfilePage,
    FriendsPage,
    ErrorPage,
    NotificationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    {
      provide: ErrorHandler, useClass: IonicErrorHandler
    },
    {
      provide: "",
      useClass: Interceptor,
      multi: true
    },
    GameProvider,
    UserProvider,
    TokenStorage,
    HttpClientModule,
    LobbyProvider,
    PlayerProvider,
    FriendsProvider,
    FcmProvider,
    NotificationProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {

}
