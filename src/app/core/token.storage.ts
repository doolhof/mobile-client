import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

const TOKEN_KEY = 'AuthToken';

@Injectable()
export class TokenStorage {

  constructor(private storage: Storage) {
  }

  signOut() {
    this.storage.remove(TOKEN_KEY);
    this.storage.remove('currentUser');
    this.storage.clear();
  }

  public saveToken(token: string) {
    this.storage.remove(TOKEN_KEY);
    this.storage.set(TOKEN_KEY, token);
  }

  public getToken(): any {
    this.storage.get(TOKEN_KEY).then(token => {
      return token.toString();
    });
  }
}
