import {Injectable} from '@angular/core';
import {NavController, NavParams} from "ionic-angular";
import {LoginPage} from "../../pages/login/login";


@Injectable()
export class AuthGuard {

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {
  }

  onActivate() {
    if (localStorage.getItem('currentUser')) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.navCtrl.push(LoginPage);
    return false;
  }
}
