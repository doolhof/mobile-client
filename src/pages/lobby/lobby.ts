import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Lobby, LobbySocketMessage, User} from "../../model/user";
import {LobbyProvider} from "../../providers/lobby/lobby";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Storage} from '@ionic/storage';
import {Howl} from 'howler';
import {MainmenuPage} from "../mainmenu/mainmenu";
import {GameProvider} from "../../providers/game/game";
import {GametimePage} from "../game/gametime";

@IonicPage()
@Component({
  selector: 'page-lobby',
  templateUrl: 'lobby.html',
})
export class LobbyPage implements OnInit {

  private lobbyStompClient;
  lobby: Lobby;
  private user: User;
  private socketMessage: LobbySocketMessage;
  private hostUser: User;
  plsNoRemoveLobby: boolean;
  lobbyId: number;
  sound = new Howl({
    src: ['assets/sounds/lobbyMusic.mp3'], html5: true,
    autoplay: true,
    loop: true,
    volume: 0.0
  });

  private showChatValue: boolean;
  private showInviteValue: boolean;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private  lobbyProvider: LobbyProvider,
              private storage: Storage, private gameProvider: GameProvider) {
    this.lobbyId = this.navParams.get('lobbyId');
    this.lobbyProvider.getLobbyById(this.lobbyId).subscribe(res => {
      this.lobby = res;
      this.hostUser = this.lobby.host;
      this.lobbyConnect();
    });

  }

  ngOnInit() {
    // this.sound.play();
    this.plsNoRemoveLobby = false;
  }

  lobbyConnect() {
    const that = this;
    const socket = new SockJS('https://doolhof-backend.herokuapp.com/game-party-socket');
    this.lobbyStompClient = Stomp.over(socket);
    this.lobbyStompClient.connect({}, function () {
      console.log(socket);
      that.lobbyStompClient.subscribe('/lobby/party', (res) => {
        if (res.body.toString() === 'true') {
          that.lobbyStompClient.disconnect();
          that.navCtrl.push(MainmenuPage);
        } else {
          that.lobby = JSON.parse(res.body);
        }
      });
      that.joinParty();
    });
  }

  joinParty() {
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
      this.socketMessage = new LobbySocketMessage(this.user.userId.toString(), this.lobby.lobbyId.toString());
      this.lobbyStompClient.send('/party/join', {}, JSON.stringify(this.socketMessage));
    });
  }

  canDeactivate() {
    if (this.user && this.lobby.host.userId === this.user.userId) {
      this.lobbyStompClient.send('/party/leaveHost', {}, JSON.stringify(this.socketMessage));
      this.lobbyStompClient.disconnect();
    } else {
      this.lobbyStompClient.send('/party/leave', {}, JSON.stringify(this.socketMessage));
      this.lobbyStompClient.disconnect();
    }
  }

  showChat() {
    this.showChatValue = !this.showChatValue;
  }

  toggleInvite() {
    this.showInviteValue = !this.showInviteValue;
  }

  onStartGame() {
    this.plsNoRemoveLobby = true;
    console.log(this.lobby.game);
    if (this.lobby.game != null) {
      this.navCtrl.push(GametimePage, this.lobby.game.gameId);
    } else {
      let game;
      this.gameProvider.getNewGame(this.lobby.lobbyId).subscribe(res => {
          game = res;
          this.navCtrl.push(GametimePage, res.gameId);
        }
      );
    }
  }

  onLeave() {
    this.navCtrl.push(MainmenuPage);
  }
}
