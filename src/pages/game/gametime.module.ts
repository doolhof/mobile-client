import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GametimePage } from './gametime';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    GametimePage,
  ],
  imports: [
    IonicPageModule.forChild(GametimePage),
    ComponentsModule
  ],
})
export class GametimePageModule {}
