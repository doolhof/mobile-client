import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Game, GameSocketMessage, MazeCard, Player, Tile, Turn} from "../../model/game";
import {User} from "../../model/user";
import {TurnStatusComponent} from "../../components/unique/game/turn-status/turn-status";
import {GameboardComponent} from "../../components/unique/game/gameboard/gameboard";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Storage} from '@ionic/storage';
import * as GameUtil from '../../util/gameUtil';
import {ScreenOrientation} from "@ionic-native/screen-orientation/ngx";

@IonicPage()
@Component({
  selector: 'page-gametime',
  templateUrl: 'gametime.html',
})
export class GametimePage {

  user: User;
  game: Game;
  remainingMazeCard: MazeCard;
  myPlayer: Player = new Player(null,"GREEN",null,null,null,null,null,null,null);
  otherPlayers: Player[];
  playerTurn: Player = new Player(null,"GREEN",null,null,null,null,null,null,null);
  turn = new Turn(10, 'CARDMOVE', new Date(), [], 0, 0, -90);
  showMenu: boolean;
  @ViewChild(GameboardComponent) gameboardComponent: GameboardComponent;
  @ViewChild(TurnStatusComponent) turnStatusComponent: TurnStatusComponent;
  gameStompClient;


  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  private url = "http://localhost:9090/";

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {

    this.gameConnect();
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)});

  }
  ionViewDidLoad() {
    this.gameConnect();
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)});
  }

  public sortTreasures() {
    this.myPlayer.toFind = this.myPlayer.toFind.sort((a, b) => b.treasureId - a.treasureId).reverse();
  }

  ngOnInit() {
    this.gameConnect();
    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user)});
    this.showMenu = false;
  }

  gameConnect() {
    const that = this;
    const socket = new SockJS(this.url + 'game-socket'); // Socket in config van back-end
    this.gameStompClient = Stomp.over(socket);

    this.gameStompClient.connect({}, function () {
      that.getGameSubscription();
      that.getPlayerSubscription();
      that.movePlayerSubscription();
      that.moveMazeCardSubscription();
      that.endTurnSubscription();
      GameUtil.getGame(2, that.user.userId, that.gameStompClient);
    });
  }


  getGameSubscription() { // Method for getGame websocket
    this.gameStompClient.subscribe('/game/gameParty', res => {
      this.game = JSON.parse(res.body);
      this.remainingMazeCard = this.game.gameBoard.tiles[49].mazeCard;
      this.myPlayer = this.game.playerList.filter(player => player.name === this.user.username)[0];
      this.otherPlayers = this.game.playerList.filter(p => this.myPlayer.playerId !== p.playerId);
      this.game.gameBoard.tiles = this.game.gameBoard.tiles.slice(0, -1);
      this.playerTurn = this.game.playerList[0];   // Players who's turn it is
      this.sortTreasures();
      this.turn.turnDuration = this.game.turnTime;
    });
  }

  getPlayerSubscription() {
    this.gameStompClient.subscribe('/game/getPlayer', res => {
      const tempPlayer = JSON.parse(res.body);
      if (tempPlayer.name === this.myPlayer.name) {
        this.myPlayer = tempPlayer;
      } else {
        for (let _i = 0; _i < this.otherPlayers.length; _i++) {
          if (this.otherPlayers[_i].name === tempPlayer.name) {
            this.otherPlayers[_i] = tempPlayer;
          }
        }
      }
    });
  }

  movePlayerSubscription() {
    this.gameStompClient.subscribe('/game/movePlayerTile', res => {
      this.game.playerList = JSON.parse(res.body);
      this.myPlayer = this.game.playerList.filter(player => player.name === this.user.username)[0];
      this.otherPlayers = this.game.playerList.filter(p => this.myPlayer.playerId !== p.playerId);
    });
  }

  moveMazeCardSubscription() {
    this.gameStompClient.subscribe('/game/moveMazeCard', res => {
      this.game.gameBoard = JSON.parse(res.body);
      this.remainingMazeCard = this.game.gameBoard.tiles[49].mazeCard;
      this.game.gameBoard.tiles = this.game.gameBoard.tiles.slice(0, -1);
    });
  }

  endTurnSubscription() { // Method for endturn websocket
    this.gameStompClient.subscribe('/game/endTurn', res => {
      this.setNextPlayerTurn();
      this.turn.playerMoves = [];
    });
  }

  onEndTurn(timeLeft: number): void {
    this.turn.turnDuration = timeLeft;
    this.turn.player = this.playerTurn.playerId;
    this.gameStompClient.send('/gameParty/endTurn', {}, JSON.stringify(this.turn));
  }

  updateTurnOrientation(orientation: number) {
    this.turn.orientation = orientation;
  }

  /**Change name*/
  onUpdateTurn(turn: Turn) {
    this.turn.turnPhase = turn.turnPhase;
    this.turn.posMazeCardMove = turn.posMazeCardMove;
    this.turn.playerMoves = turn.playerMoves;
  }

  setNextPlayerTurn() {
    const x = GameUtil.getNextPlayer(this.game.playerList, this.playerTurn);
    this.game.playerList[x].isTurn = true;
    this.playerTurn = this.game.playerList[x];
    this.turn.turnDuration = this.game.turnTime;
    this.gameboardComponent.updateTurn();
  }

  onMenu() {
    this.showMenu = true;
  }

  onBack() {
    this.showMenu = false;
  }



}
