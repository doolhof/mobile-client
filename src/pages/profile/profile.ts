import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {User} from "../../model/user";
import {MainmenuPage} from "../mainmenu/mainmenu";
import {Storage} from '@ionic/storage';
import {TokenStorage} from "../../app/core/token.storage";
import {LobbyProvider} from "../../providers/lobby/lobby";


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {
 private user: User;
  incomingMessage : any;
  myTurn : any;
  turnExpiring : any;
  receiveInvite : any;
  turnPlayed : any;
  showEditPassword: boolean;
  showDeleteAccount: boolean;

  constructor(private  userProvider: UserProvider,
              private tokenstorage: TokenStorage,
              private lobbyProvider: LobbyProvider,
              public navCtrl: NavController,
              navParams: NavParams,
              private storage: Storage) {
    this.storage.get('currentUser').then(res => {

      this.user = JSON.parse(res);
      console.log(this.user);

      this.incomingMessage = this.user.userSetting.incomingMessage;
      this.myTurn = this.user.userSetting.myTurn;
      this.turnExpiring = this.user.userSetting.turnExpiring;
      this.receiveInvite = this.user.userSetting.receiveInvite;
      this.turnPlayed = this.user.userSetting.turnPlayed;
    });
  }

  ngOnInit() {
    this.storage.get('currentUser').then(res => {
       this.user = JSON.parse(res);
      console.log(this.user);

      this.incomingMessage = this.user.userSetting.incomingMessage;
      this.myTurn = this.user.userSetting.myTurn;
      this.turnExpiring = this.user.userSetting.turnExpiring;
      this.receiveInvite = this.user.userSetting.receiveInvite;
      this.turnPlayed = this.user.userSetting.turnPlayed;
    });
  }

    checkClickedMyTurn(val) {
    if (val) {
      this.myTurn = false;
      this.user.userSetting.myTurn = this.myTurn;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.myTurn = true;
      this.user.userSetting.myTurn = this.myTurn;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }

  checkClickedIncomingMessage(val) {
    if (val) {
      this.incomingMessage = false;
      this.user.userSetting.incomingMessage = this.incomingMessage;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.incomingMessage = true;
      this.user.userSetting.incomingMessage = this.incomingMessage;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
      });
    }
  }

  checkClickedTurnExpiring(val) {
    if (val) {
      this.turnExpiring = false;
      this.user.userSetting.turnExpiring = this.turnExpiring;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.turnExpiring = true;
      this.user.userSetting.turnExpiring = this.turnExpiring;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }

  checkClickedReceiveInvite(val) {
    if (val) {
      this.receiveInvite = false;
      this.user.userSetting.receiveInvite = this.receiveInvite;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.receiveInvite = true;
      this.user.userSetting.receiveInvite = this.receiveInvite;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }

  checkClickedTurnPlayed(val) {
    if (val) {
      this.turnPlayed = false;
      this.user.userSetting.turnPlayed = this.turnPlayed;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.turnPlayed = true;
      this.user.userSetting.turnPlayed = this.turnPlayed;
      this.userProvider.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }

  onChangePassword() {
    this.showEditPassword = true;
  }

  onDeleteAccount() {
    this.showDeleteAccount = true;
  }

  onBack() {
    this.showEditPassword = false;
    this.showDeleteAccount = false;
  }

  onBackMain() {
    this.navCtrl.push(MainmenuPage);
  }

  updateUser() {
    this.userProvider.getUserByUserId(this.user.userId).subscribe(res => {
      this.storage.set('currentUser', JSON.stringify(res));
    });
  }
}
