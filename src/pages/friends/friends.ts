import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FriendsProvider} from "../../providers/friends/friends";
import {UserProvider} from "../../providers/user/user";
import {User} from "../../model/user";
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {
  user: User;
  friends: User[];
  selectedFriend: User;
  showAddMenu: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserProvider,
              private friendService: FriendsProvider, private  storage: Storage) {



    this.storage.get('currentUser').then(res => {
      this.user = JSON.parse(res)
      this.friendService.getFriendsByUser(this.user.userId).subscribe(res => this.friends = res);
    });
  }

  ngOnInit() {
    this.showAddMenu = false;

  }

  onSelect(friend: User) {
    this.selectedFriend = friend;
  }

  onUnselect() {
    this.selectedFriend = null;
  }

  onRemove(friend: User) {
    this.friendService.removeFriendByUser(this.user.userId, friend.userId).subscribe(res => {
      this.user = res;
      this.friends = res.friends;
    });
  }

  onBack() {
    this.navCtrl.pop();
  }

  onUpdateUser() {
    this.userService.getUserByUserId(this.user.userId).subscribe(res => this.user = res);
    this.friendService.getFriendsByUser(this.user.userId).subscribe(res => {
      this.friends = res;
      console.log(this.user);
      console.log(this.friends);
    });
  }

  onOpenAddMenu() {
    this.showAddMenu = true;
  }

  onMenuBack() {
    this.showAddMenu = false;
  }
}
