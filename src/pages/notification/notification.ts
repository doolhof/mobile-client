import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireMessaging} from "@angular/fire/messaging";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireDatabase} from "@angular/fire/database";
import {BehaviorSubject} from "rxjs";
import {Notificat, User} from "../../model/user";
import {take} from "rxjs/operators";
import {NotificationProvider} from "../../providers/notification/notification";
import {Storage} from "@ionic/storage";
import {MainmenuPage} from "../mainmenu/mainmenu";

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  message;
  inlogUser: User;
  currentMessage = new BehaviorSubject(null);
  notificationList: Notificat[];
  showNotifications: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,private notificationProvider: NotificationProvider) {

  }

  ngOnInit() {
    this.storage.get('currentUser').then(user => {
      this.inlogUser = JSON.parse(user);
      console.log(this.inlogUser);
      this.getNotifications();
    });
    this.showNotifications = false;
  //  this.getNotifications();
    console.log('in ngOnInit');
    this.message = this.currentMessage;
    // this.messagingService.requestPermission(userId);
    //  this.messagingService.receiveMessage();
    // this.message = this.messagingService.currentMessage;
  }


  getNotifications() {
    this.notificationProvider.getAllNotifications(this.inlogUser.userId).subscribe(res => {
      this.notificationList = res;
    });
  }

  onShowNotifications() {
    console.log('in onShowNotifications');
    this.showNotifications = true;
  }

  onClose() {
   this.navCtrl.push(MainmenuPage);
  }

}
