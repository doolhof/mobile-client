import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../model/user";
import {HttpErrorResponse} from "@angular/common/http";
import {UserProvider} from "../../providers/user/user";
import {Token} from "@angular/compiler";
import {RegistrationSuccesPage} from "./registration-succes/registration-succes";
import {LoginPage} from "../login/login";

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  user: User;
  email: string;
  password: string;
  passwordConf: string;
  invalidEmail: string;
  invalidPassword: string;
  username: string;
  token: Token;

  constructor(public navCtrl: NavController, public navParams: NavParams,private  userService: UserProvider) {
  }


  ngOnInit() {
  }

  readUsername(username): void {
    console.log('readusername');
    this.username = username;
  }

  readPassword(password): void {
    console.log('readpassword');
    this.password = password;
  }

  readPasswordConfirm(password): void {
    console.log('readpasswordconfirm');
    this.passwordConf = password;
    this.checkSamePassword();
  }

  checkSamePassword(): void {
    if (this.password === this.passwordConf) {
      this.invalidPassword = '';
    } else {
      this.invalidPassword = 'password is not the same';
    }
  }

  readMail(mail): void {
    console.log('readmail');
    this.email = mail;
    this.invalidEmail = '';
  }

  registrate(): void {
    console.log('registrate');
    this.user = new User(33, this.username, null, this.email, this.passwordConf, null, null, null, null, null);
    if (this.invalidEmail === '' && this.invalidPassword === '') {
      try {
        this.userService.postSignUp(this.user).subscribe(res => {
          if (!(res instanceof HttpErrorResponse) && res !== null) {
            this.navCtrl.push(RegistrationSuccesPage);
          }
        });
      } catch (e) {
        console.log('error by registration: ' + e);
      }
    } else {
      console.log('wrong validation');
    }
  }
  login(): void {
    this.navCtrl.push(LoginPage);
  }
}
