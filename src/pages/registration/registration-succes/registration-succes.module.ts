import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationSuccesPage } from './registration-succes';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  declarations: [
    RegistrationSuccesPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationSuccesPage),
    ComponentsModule
  ],
})
export class RegistrationSuccesPageModule {}
