import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../../providers/user/user";
import {HttpErrorResponse} from "@angular/common/http";
import {LoginPage} from "../../login/login";



@IonicPage()
@Component({
  selector: 'page-registration-succes',
  templateUrl: 'registration-succes.html',
})
export class RegistrationSuccesPage {

  constructor( private navCtrl: NavController) {

  }


  goToLogin() {
    this.navCtrl.push(LoginPage);
  }
}
