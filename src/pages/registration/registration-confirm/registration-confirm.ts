import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../../providers/user/user";
import {LoginPage} from "../../login/login";
import {HttpErrorResponse} from "@angular/common/http";

/**
 * Generated class for the RegistrationConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration-confirm',
  templateUrl: 'registration-confirm.html',
})
export class RegistrationConfirmPage {
  token: string;
  confirmed: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserProvider) {
    /*this.route.queryParams.subscribe(params => {
       this.token = params['token'];
     });*/
  }

  ngOnInit() {
    this.confirmed = false;
  }

  confirm() {
    console.log('confirm click' + this.token);
     this.userService.postConfirm(this.token).subscribe(res => {
         console.log(res);
         this.confirmed = !(res instanceof HttpErrorResponse);
       },
       error => {
         console.log('error');
       });
  }

  toSignIn() {
    this.navCtrl.push(LoginPage);
  }
}
