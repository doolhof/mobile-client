import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationConfirmPage } from './registration-confirm';
import {ComponentsModule} from "../../../components/components.module";

@NgModule({
  declarations: [
    RegistrationConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationConfirmPage),
    ComponentsModule
  ],
})
export class RegistrationConfirmPageModule {}
