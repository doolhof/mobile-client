import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {ScreenOrientation} from "@ionic-native/screen-orientation/ngx";
import {Storage} from '@ionic/storage';
import {UserProvider} from "../../providers/user/user";
import {ProfilePage} from "../profile/profile";
import {LoginPage} from "../login/login";
import {NotificationPage} from "../notification/notification";
import {User} from "../../model/user";
import {FriendsPage} from "../friends/friends";
import {FcmProvider} from "../../providers/fcm/fcm";
import {AngularFireDatabase} from "@angular/fire/database";
import {AngularFireAuth} from "@angular/fire/auth";
import {AngularFireMessaging} from "@angular/fire/messaging";
import {take} from "rxjs/operators";
import {BehaviorSubject} from "rxjs";


@IonicPage()
@Component({
  selector: 'page-mainmenu',
  templateUrl: 'mainmenu.html',
})
export class MainmenuPage implements OnInit {
  @Input() newGameValue = false;
  @Input() settingsValue = false;
  @Output() newGame = new EventEmitter();
  @Input() loadGames = new EventEmitter();
  @Output() loadGamesValue = false;
  loggedIn: boolean;
  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, private screenOrientation: ScreenOrientation,
              private storage: Storage, private userProvider: UserProvider, private fcmProvider: FcmProvider) {
    //   this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.loggedIn = userProvider.isLoggedIn();
    this.fcmProvider.ngOnInit();

  }

  ngOnInit() {

    this.storage.get('currentUser').then(user => {
      this.user = JSON.parse(user);
      console.log(this.user);
      this.loggedIn = true;
    });

  }

  newGameClick() {
    this.newGameValue = !this.newGameValue;
    this.newGame.emit(this.newGameValue);
  }

  changeToFalse(event) {
    this.newGameValue = event;
    this.loadGamesValue = event;
  }

  onLoadGames() {
    this.loadGamesValue = !this.loadGamesValue;
    this.loadGames.emit(this.loadGamesValue);
  }

  profileClick() {
    this.navCtrl.push(ProfilePage);
  }

  onLogin() {
    this.navCtrl.push(LoginPage);
  }

  onSignout() {
    this.userProvider.logout();
    this.loggedIn = false;
  }

  onFriends() {
      this.navCtrl.push(FriendsPage);

  }

  onNotification() {
    this.navCtrl.push(NotificationPage);
  }


}
