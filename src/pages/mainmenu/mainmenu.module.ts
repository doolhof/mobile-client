import { NgModule } from '@angular/core';
import { IonicPageModule} from 'ionic-angular';
import { MainmenuPage } from './mainmenu';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    MainmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(MainmenuPage),
    ComponentsModule
  ],
})
export class MainmenuPageModule {}
