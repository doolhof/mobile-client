export enum Color {
  RED, GREEN, BLUE, YELLOW
}

export class Game {
  gameId: number;
  startDate: Date;
  time: Date;
  gameBoard: GameBoard;
  playerList: Player[];
  turnTime: number;

  constructor(args: {
    gameId: number,
    startDate: Date,
    time: Date,
    gameBoard: GameBoard,
    playerList: Player[],
    turnTime: number;
  }) {
    this.gameId = args.gameId;
    this.startDate = args.startDate;
    this.time = args.time;
    this.gameBoard = args.gameBoard;
    this.playerList = args.playerList;
    this.turnTime = args.turnTime;
  }
}

export class Tile {
  tileId: number;
  position: number;
  mazeCard: MazeCard;
  moveable: boolean;

  constructor(args: {
    tileId: number,
    position: number,
    mazeCard: {
      mazeCardId: number;
      orientation: number;
      northWall: boolean;
      eastWall: boolean;
      southWall: boolean;
      westWall: boolean;
      imageType: number;
      treasure: Treasure;
    },
    moveable: boolean,
  }) {
    this.tileId = args.tileId;
    this.position = args.position;
    this.mazeCard = new MazeCard(args.mazeCard);
    this.moveable = args.moveable;
  }
}

export class GameBoard {
  gameBoardId: number;
  tiles: Tile[];

  constructor(args: {
    gameBoardId: number,
    tiles: {
      tileId: number,
      position: number,
      mazeCard: {
        mazeCardId: number;
        orientation: number;
        northWall: boolean;
        eastWall: boolean;
        southWall: boolean;
        westWall: boolean;
        imageType: number;
        treasure: Treasure;
      },
      moveable: boolean,
    }[]
  }) {
    this.gameBoardId = args.gameBoardId;
    this.tiles = args.tiles.map(json => new Tile(json));
  }
}

export class MazeCard {
  mazeCardId: number;
  orientation: number;
  northWall: boolean;
  eastWall: boolean;
  southWall: boolean;
  westWall: boolean;
  imageType: number;
  treasure: Treasure;

  constructor(args: {
    mazeCardId: number;
    orientation: number;
    northWall: boolean;
    eastWall: boolean;
    southWall: boolean;
    westWall: boolean;
    imageType: number;
    treasure: Treasure;
  }) {
    this.mazeCardId = args.mazeCardId;
    this.orientation = args.orientation;
    this.northWall = args.northWall;
    this.eastWall = args.eastWall;
    this.southWall = args.southWall;
    this.westWall = args.westWall;
    this.imageType = args.imageType;
    this.treasure = args.treasure;
  }
}

export class Player {
  playerId: number;
  color: string;
  name: string;
  toFind: Treasure[];
  found: Treasure[];
  turns: Turn[];
  currentPosition: number;
  startPosition: number;
  isTurn: boolean;

  constructor(
    playerId: number,
    color: string,
    name: string,
    toFind: Treasure[],
    found: Treasure[],
    turns: Turn[],
    currentTile: number,
    startTile: number,
    isTurn: boolean) {
    this.playerId = playerId;
    this.color = color;
    this.name = name;
    this.toFind = toFind;
    this.found = found;
    this.turns = turns;
    this.currentPosition = currentTile;
    this.startPosition = startTile;
    this.isTurn = isTurn;
  }
}

export class Treasure {
  treasureId: number;
  name: string;

  constructor(treasureId: number, name: string) {
    this.treasureId = this.treasureId;
    this.name = name;
  }
}

export class Turn {
  // turnId: number;
  turnDuration: number;
  turnPhase: string;
  turnDate: Date;
  playerMoves: number[];
  player: number;
  posMazeCardMove: number;
  orientation: number;


  constructor(turnDuration: number, turnPhase: string, turnDate: Date, playerMoves: number[],
              player: number, posMazeCardMove: number, orientation: number) {
    // this.turnId = turnId;
    this.turnDuration = turnDuration;
    this.turnPhase = turnPhase;
    this.turnDate = turnDate;
    this.playerMoves = playerMoves;
    this.player = player;
    this.posMazeCardMove = posMazeCardMove;
    this.orientation = orientation;
  }
}

/**
 * gameMessage for websSocket*/
export class GameSocketMessage {
  gameId: string;
  userId: string;

  constructor(gameId: string, userId: string) {
    this.gameId = gameId;
    this.userId = userId;
  }
}

/**
 * PlayerMessage for webSockets*/
export class PlayerSocketMessage {
  playerId: string;
  destination: string;

  constructor(playerId: string, destination: string) {
    this.playerId = playerId;
    this.destination = destination;
  }
}

/**
 * TileMessage for webSockets*/
export class TileSocketMessage {
  position: string;
  gameBoardId: string;

  constructor(position: string, gameBoardId: string) {
    this.position = position;
    this.gameBoardId = gameBoardId;
  }
}
