import {Game} from './game';

export class Community {
  listUsers: User[];
}

export class GameInvite {
  inviteId: number;
  lobby: Lobby;
}

export class Lobby {
  lobbyId: number;
  listUsers: User[];
  gameSettings: GameSettings;
  host: User;
  game: Game;
  constructor(lobbyId: number, listUsers: User[], gameSettings: GameSettings, host: User, game: Game) {
    this.lobbyId = lobbyId;
    this.listUsers = listUsers;
    this.gameSettings = gameSettings;
    this.host = host;
    this.game = game;
  }
}


export class GameSettings {
  gameSettingId: number;
  amountOfUsers: number;
  secondsPerTurn: number;

  constructor(
    gameSettingId: number,
    amountOfUsers: number,
    secondsPerTurn: number,
  ) {
    this.gameSettingId = gameSettingId;
    this.amountOfUsers = amountOfUsers;
    this.secondsPerTurn = secondsPerTurn;
  }
}

export enum Role {
  ROLE_ADMIN = <any>'ROLE_ADMIN', ROLE_CLIENT = <any>'ROLE_CLIENT'
}

/*export class User {
  userId: number;
  username: string;
  friends: User[];
  email: string;
  encryptedPassword: string;
  lobbies: Lobby[];
  userStatistic: UserStatistic;
  userSetting: UserSetting;
  roles: Role[];

  constructor(userId: number,
              username: string,
              friends: User[],
              email: string,
              encryptedPassword: string,
              lobbies: Lobby[],
              userStatistic: UserStatistic,
              userSetting: UserSetting) {
    this.userId = userId;
    this.username = username;
    this.friends = friends;
    this.email = email;
    this.encryptedPassword = encryptedPassword;
    this.lobbies = lobbies;
    this.userStatistic = userStatistic;
    this.userSetting = userSetting;
  }


}*/
export class User {
  userId: number;
  username: string;
  friends: User[];
  email: string;
  encryptedPassword: string;
  lobbies: Lobby[];
  userStatistic: UserStatistic;
  userSetting: UserSetting;
  notifications: Notificat[];
  fcmToken: string;

  roles: Role[];

  constructor(userId: number,
              username: string,
              friends: User[],
              email: string,
              encryptedPassword: string,
              lobbies: Lobby[],
              userStatistic: UserStatistic,
              userSetting: UserSetting,
              notifictions: Notificat[],
              fcmToken: string) {
    this.userId = userId;
    this.username = username;
    this.friends = friends;
    this.email = email;
    this.encryptedPassword = encryptedPassword;
    this.lobbies = lobbies;
    this.userStatistic = userStatistic;
    this.userSetting = userSetting;
    this.notifications = notifictions;
    this.fcmToken = fcmToken;
  }


}

export class UserSetting {
  settingId: number;
  receiveInvite: boolean;
  turnPlayed: boolean;
  myTurn: boolean;
  turnExpiring: boolean;
  incomingMessage: boolean;
}

export class UserStatistic {
  statisticId: number;
  gamesPlayed: number;
  gamesWon: number;
  gamesLost: number;
}


export class Token {
  token: string;
  user: User;
  constructor(token: string, user: User) {
    this.token = token;
    this.user = user;
  }
}

export class FbInfo {
  facebookInfo: string;
  constructor(facebookInfo: string) {
    this.facebookInfo = facebookInfo;
  }
}

export class PasswordDto {
  userNumber: number;
  oldPassword: string;
  newPassword: string;

  constructor(userNumber: number, oldPassword: string, newPassword: string ) {
    this.userNumber = userNumber;
    this.oldPassword = oldPassword;
    this.newPassword = newPassword;
  }
}

export class LobbySocketMessage {
  userId: string;
  lobbyId: string;
  constructor(userId: string, lobbyId: string) {
    this.userId = userId;
    this.lobbyId = lobbyId;
  }
}

export class ChatSocketMessage {
  userId: string;
  username: string;
  message: string;


  constructor(userId: string, username: string, message: string) {
    this.userId = userId;
    this.username = username;
    this.message = message;
  }
}
export class Notificat {
  notificationId: number;
  notifDesc: string;
  user: User;
  notificationType: string;
  webUrl: string;
  mobileUrl: string;
  closed: boolean;
  constructor(notificationId: number, notifDesc: string, user: User, notificationType: string, webUrl: string,
              mobileUrl: string, closed: boolean) {
    this.notificationId = notificationId;
    this.notifDesc = notifDesc;
    this.user = user;
    this.notificationType = notificationType;
    this.webUrl = webUrl;
    this.mobileUrl = mobileUrl;
    this.closed = closed;
  }
}
