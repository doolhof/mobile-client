// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '250908293848'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
console.log(messaging.getToken());

if('serviceWorker' in navigator){
  navigator.serviceWorker.register('firebase-messaging.sw.js')
    .then(function(registration){
      console.log('Registration sucess, scope: ', registration.scope);
    }).catch(function(err){
      console.log('failed', err);
  });
}
/*importScripts('https://www.gstatic.com/firebasejs/5.2.0/firebase.js');
importScripts('https://www.gstatic.com/firebasejs/5.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.2.0/firebase-messaging.js');

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyD2hHT_qHQe6hfbglnFnlbkq9dvqYZ9iOE',
  authDomain: 'mazetrix-c09ac.firebaseapp.com',
  databaseURL: 'https://mazetrix-c09ac.firebaseio.com',
  projectId: 'mazetrix-c09ac',
  storageBucket: 'mazetrix-c09ac.appspot.com',
  messagingSenderId: '250908293848'
};

firebase.initializeApp(config);

var messaging = firebase.messaging();
console.log(messaging.getToken());
*/
